import sys
clients = [
	{
		'name': 'Pablo',
		'company': 'Google',
		'email': 'pablo@google.com',
		'position': 'Software engineer',
	},
	{
		'name': 'Ricardo',
		'company': 'Facebook',
		'email': 'ricardo@facebook.com',
		'position': '',
	}
] #creamos el string

def _print_welcome():
	#print ('Welcome to Monares POS')
	print ('what would you like to do today?')
	print ('*' * 52)  #imprimimos el caracter "*" 52 veces
	print ('[C]reate client')
	print ('[L]ist client')
	print ('[U]pdate client')
	print ('[D]elete client')
	print ('[S]earch client')


def create_client(client_name):
	global clients  #Utilizamos global para definir que la variable es la globarl, es decir la que definimos con pablo y ricardo
	#convertimos la cadena de texto en minusculas y la guardamos en otra variable para no diferenciar entre "Pablo" y "pablo"
	if client_name not in clients: #si el nombre del cliente no esta en clients_minusculas
		
		clients.append(client_name)
		 
	else:
		print('Client already exists') #Sino mostramos un mensaje al usuario


def update_client(client_name, update_name):
	global clients
	if client_name in clients:
		index = clients.index(client_name)
		clients[index] = update_name
	else:
		print ('Client is not in client list')

def delete_client(client_name):
	global clients

	if client_name in clients:
		clients.remove(client_name)
	else:
		print('Client is not in clients list')

def search_client(client_name):
	
	for client in clients:
		if client != client_name:
			continue
		else:
			return True

def list_clients():#función que muestra la lista de clientes
	for idx, client in enumerate(clients):
		print('{uid} | {name} | {company} | {email} | {position}'.format(
			uid = idx,
			name = client['name'],
			company = client['company'],
			email = client['email'],
			position = client['position']))


def _get_client_field(fiel_name):
	field = None

	while not field:
		field = input('What is the client {}?'.format(fiel_name))
	return field

def _get_client_name():
	client_name = None

	while not client_name:
		client_name = input('What is the client name?')
		
		if client_name == 'exit':
			client_name=None
			break

	if not client_name:
			sys.exit()	
	return client_name  


if __name__ == '__main__': #funcion main
	_print_welcome() #ejecutamos la funcion que da el mensaje de bienvenida
	command = input() #guardamos el valor del dato ingresado en la variable command pero lo convertimos a miunsculas para realizar la accion si presiona "C" o "c"
	if command == 'C': #si el comando es igual al caracter "c"
		client = {
			'name': _get_client_field('name'), 
			'company': _get_client_field('company'),
			'email': _get_client_field('email'),
			'position': _get_client_field('position'),
		}

		create_client(client) #ejecutamos la funcion crear cliente y enviamos como parametro el valor de la variable que almacena lo que digitó el usuario
		list_clients() #Ejecutamos la funcion listar clientes 
	elif command == 'U':
		client_name = _get_client_name()
		update_client(client_name)
		list_clients()
	elif command == 'D': #si el dato ignresado por el usuario es "d"
		client_name = _get_client_name()
		delete_client(client_name)
		list_clients()
	elif command == 'L': #si el dato ignresado por el usuario es "d"
		list_clients()	
	elif command == 'S': #si el dato ignresado por el usuario es "d"
		client_name = _get_client_name()
		found = search_client(client_name)
		if found:
			print('The client is in the client\'s list')
		else:
			print('The client: {} is not our client\'s list'.format(client_name))
	else:
		print('Invalid command')
input() #Escribimos un input para que el programa haga una pausa y no cierre la ventana hasta recibir un enter